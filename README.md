# FuseCDI

This is a FUSE filesystem driver for mounting Discjuggler CDI images. This format is commonly used for images of Dreamcast games. 

Windows has proprietary software for mounting CDI images, I've written this library so that Linux users can also have the same
functionality.