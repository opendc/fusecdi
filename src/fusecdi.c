#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/vfs.h>

#define FUSE_USE_VERSION 26

#include <fuse.h>

typedef struct {
    uint8_t reserved[14];
    uint8_t control;
    uint8_t mode; // 0 - Audio, 1 - Mode1/DVD, 2 - Mode2
    uint8_t ISRC[12];
    bool is_ISRC_valid;
    uint32_t block_size;
    uint32_t index_count;
    uint32_t* indexes;
#if __x86_64__
#else
    uint32_t padding_for_32bit;
#endif
} Track;

typedef struct {
    uint8_t reserved[3];
    uint8_t type; // 0 - CD-DA, 1 - CDROM, 2 - CD-XA, 3 - CD-I, 4 - Open
    uint32_t track_count;
    Track* tracks;
#if __x86_64__
#else
    uint32_t padding_for_32bit;
#endif
} Session;

struct TableOfContents {
    uint8_t version;
    uint8_t first_session_number;
    uint8_t first_track_number;
    uint8_t universal_product_code;
    uint32_t univeral_product_code_valid :1;
    uint32_t is_dvd :1;
    uint32_t reserved :30;

    uint32_t text_size_in_bytes; // CD-Text size, multiple of 18
    uint8_t* text;
#if __x86_64__
#else
    uint32_t padding_for_32bit;
#endif

    uint32_t highest_possible_lead_out_address_in_blocks;
    uint32_t total_session_count;

    Session* sessions;
#if __x86_64__
#else
    uint32_t more_padding_for_32bit;
#endif

};

int cdi_getattr(const char *path, struct stat *stbuf) {

}

int cdi_readlink(const char *path, char *target, size_t size) {

}

int cdi_open(const char* path, struct fuse_file_info* info) {

}

int cdi_read(const char* path, char* buf, size_t size, off_t offset, struct fuse_file_info* info) {

}

int cdi_statfs(const char *path, struct statvfs *stbuf) {

}

int cdi_flush(const char *path, struct fuse_file_info *info) {

}

int cdi_opendir(const char *path, struct fuse_file_info *info) {
    fprintf(stderr, "opendir...\n");
}

int cdi_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *info) {
    fprintf(stderr, "readdir...\n");
}

void* cdi_init(struct fuse_conn_info *conn) {
    fprintf(stderr, "Initializing...\n");
}

void cdi_destroy(void* param) {

}

struct fuse_operations cdi_oper = {
    .getattr = cdi_getattr,
    .readlink = cdi_readlink,
    .open = cdi_open,
    .read = cdi_read,
    .statfs = cdi_statfs,
    .flush = cdi_flush,
    .opendir = cdi_opendir,
    .readdir = cdi_readdir,
    .init = cdi_init,
    .destroy = cdi_destroy
};

typedef struct {
    char* image_file;
} CDIState;

int main(int argc, char* argv[]) {
    CDIState state;

    bool is_root = (getuid() == 0 || geteuid() == 0);

    if(is_root) {
        fprintf(stderr, "Mounting as root is not supported for security reasons\n");
        return 1;
    }

    struct fuse_args args = FUSE_ARGS_INIT(0, NULL);

    for(int i = 0; i < argc; i++) {
        if (i == 0) {
            state.image_file = argv[i];
        } else {
            fuse_opt_add_arg(&args, argv[i]);
        }
    }

    return fuse_main(args.argc, args.argv, &cdi_oper, &state);
}
